package cn.pyuu.monitor;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

/**
 * 监控中心
 * 
 * @author pyuu
 */
@EnableAdminServer
@SpringCloudApplication
public class PyuuMonitorApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(PyuuMonitorApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  认证授权中心启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                " .-------.       ____     __        \n" +
                " |  _ _   \\      \\   \\   /  /    \n" +
                " | ( ' )  |       \\  _. /  '       \n" +
                " |(_ o _) /        _( )_ .'         \n" +
                " | (_,_).'          (_ o _)'        \n" +
                " |  |          ||   |(_,_)'         \n" +
                " |  |          |   `-'  /           \n" +
                " |  |           \\      /           \n" +
                " ''-'           `-..-'              ");
    }
}
