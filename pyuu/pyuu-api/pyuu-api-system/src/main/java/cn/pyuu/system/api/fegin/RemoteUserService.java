package cn.pyuu.system.api.fegin;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import cn.pyuu.common.core.constant.ServiceNameConstants;
import cn.pyuu.common.core.domain.R;
import cn.pyuu.system.api.fegin.factory.RemoteUserFallbackFactory;
import cn.pyuu.system.api.vo.UserInfo;

/**
 * 用户服务
 * 
 * @author pyuu
 */
@FeignClient(contextId = "remoteUserService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteUserFallbackFactory.class)
public interface RemoteUserService
{
    /**
     * 通过用户名查询用户信息
     *
     * @param username 用户名
     * @return 结果
     */
    @GetMapping(value = "/user/info/{username}")
    public R<UserInfo> getUserInfo(@PathVariable("username") String username);
}
