package cn.pyuu.gateway.service;

import java.io.IOException;
import cn.pyuu.common.core.exception.CaptchaException;
import cn.pyuu.common.core.web.domain.AjaxResult;

/**
 * 验证码处理
 * 
 * @author pyuu
 */
public interface ValidateCodeService
{
    /**
     * 生成验证码
     */
    public AjaxResult createCapcha() throws IOException, CaptchaException;

    /**
     * 校验验证码
     */
    public void checkCapcha(String key, String value) throws CaptchaException;
}
