package cn.pyuu.gateway;

import cn.pyuu.gateway.config.KaptchaTextCreator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

/**
 * 网关启动程序
 * 
 * @author pyuu
 */
@EnableDiscoveryClient
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class PyuuGatewayApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(PyuuGatewayApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  认证授权中心启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                " .-------.       ____     __        \n" +
                " |  _ _   \\      \\   \\   /  /    \n" +
                " | ( ' )  |       \\  _. /  '       \n" +
                " |(_ o _) /        _( )_ .'         \n" +
                " | (_,_).'          (_ o _)'        \n" +
                " |  |          ||   |(_,_)'         \n" +
                " |  |          |   `-'  /           \n" +
                " |  |           \\      /           \n" +


                " ''-'           `-..-'              ");
    }
}