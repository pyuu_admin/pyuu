package cn.pyuu.common.core.exception;

/**
 * 演示模式异常
 * 
 * @author pyuu
 */
public class DemoModeException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public DemoModeException()
    {
    }
}
