package cn.pyuu.common.core.exception.user;

import cn.pyuu.common.core.exception.BaseException;

/**
 * 用户信息异常类
 * 
 * @author pyuu
 */
public class UserException extends BaseException
{
    private static final long serialVersionUID = 1L;

    public UserException(String code, Object[] args)
    {
        super("user", code, args, null);
    }
}
