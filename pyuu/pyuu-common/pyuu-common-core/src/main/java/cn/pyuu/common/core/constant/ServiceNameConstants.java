package cn.pyuu.common.core.constant;

/**
 * 服务名称
 * 
 * @author pyuu
 */
public class ServiceNameConstants
{
    /**
     * 认证服务的serviceid
     */
    public static final String AUTH_SERVICE = "pyuu-auth";

    /**
     * 系统模块的serviceid
     */
    public static final String SYSTEM_SERVICE = "pyuu-system";
}
