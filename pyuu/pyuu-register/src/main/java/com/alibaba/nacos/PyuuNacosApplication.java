package com.alibaba.nacos;

import com.alibaba.nacos.console.config.ConfigConstants;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author nacos
 * <p>
 * nacos console 源码运行，方便开发
 * 生产建议从官网下载最新版配置运行
 */
@EnableScheduling
@SpringBootApplication
public class PyuuNacosApplication {

	public static void main(String[] args) {
		System.setProperty(ConfigConstants.TOMCAT_DIR, "logs");
		System.setProperty(ConfigConstants.TOMCAT_ACCESS_LOG, "false");
		System.setProperty(ConfigConstants.STANDALONE_MODE, "true");
		SpringApplication.run(PyuuNacosApplication.class, args);
		System.out.println("(♥◠‿◠)ﾉﾞ  认证授权中心启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
				" .-------.       ____     __        \n" +
				" |  _ _   \\      \\   \\   /  /    \n" +
				" | ( ' )  |       \\  _. /  '       \n" +
				" |(_ o _) /        _( )_ .'         \n" +
				" | (_,_).'          (_ o _)'        \n" +
				" |  |          ||   |(_,_)'         \n" +
				" |  |          |   `-'  /           \n" +
				" |  |           \\      /           \n" +
				" ''-'           `-..-'              ");
	}
}
