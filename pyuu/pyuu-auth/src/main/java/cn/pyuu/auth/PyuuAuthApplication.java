package cn.pyuu.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import cn.pyuu.common.security.annotation.EnableRyFeignClients;

/**
 * 认证授权中心
 * 
 * @author pyuu
 */
@EnableRyFeignClients
@SpringCloudApplication
public class PyuuAuthApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(PyuuAuthApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  认证授权中心启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                " .-------.       ____     __        \n" +
                " |  _ _   \\      \\   \\   /  /    \n" +
                " | ( ' )  |       \\  _. /  '       \n" +
                " |(_ o _) /        _( )_ .'         \n" +
                " | (_,_).'          (_ o _)'        \n" +
                " |  |          ||   |(_,_)'         \n" +
                " |  |          |   `-'  /           \n" +
                " |  |           \\      /           \n" +
                " ''-'           `-..-'              ");
    }
}
