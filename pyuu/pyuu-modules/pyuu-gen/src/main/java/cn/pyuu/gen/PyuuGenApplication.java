package cn.pyuu.gen;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import cn.pyuu.common.security.annotation.EnableCustomConfig;
import cn.pyuu.common.security.annotation.EnableRyFeignClients;
import cn.pyuu.common.swagger.annotation.EnableCustomSwagger2;

/**
 * 代码生成
 * 
 * @author pyuu
 */
@EnableCustomConfig
@EnableCustomSwagger2   
@EnableRyFeignClients
@SpringCloudApplication
public class PyuuGenApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(PyuuGenApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  认证授权中心启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                " .-------.       ____     __        \n" +
                " |  _ _   \\      \\   \\   /  /    \n" +
                " | ( ' )  |       \\  _. /  '       \n" +
                " |(_ o _) /        _( )_ .'         \n" +
                " | (_,_).'          (_ o _)'        \n" +
                " |  |          ||   |(_,_)'         \n" +
                " |  |          |   `-'  /           \n" +
                " |  |           \\      /           \n" +
                " ''-'           `-..-'              ");
    }
}
