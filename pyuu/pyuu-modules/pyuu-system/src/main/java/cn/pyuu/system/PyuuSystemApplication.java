package cn.pyuu.system;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import cn.pyuu.common.security.annotation.EnableCustomConfig;
import cn.pyuu.common.security.annotation.EnableRyFeignClients;
import cn.pyuu.common.swagger.annotation.EnableCustomSwagger2;

/**
 * 系统模块
 * 
 * @author pyuu
 */
@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringCloudApplication
public class PyuuSystemApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(PyuuSystemApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  认证授权中心启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                " .-------.       ____     __        \n" +
                " |  _ _   \\      \\   \\   /  /    \n" +
                " | ( ' )  |       \\  _. /  '       \n" +
                " |(_ o _) /        _( )_ .'         \n" +
                " | (_,_).'          (_ o _)'        \n" +
                " |  |          ||   |(_,_)'         \n" +
                " |  |          |   `-'  /           \n" +
                " |  |           \\      /           \n" +
                " ''-'           `-..-'              ");
    }
}
